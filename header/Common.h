//
//  Common.h
//  Common
//
//  Created by wangming on 16/7/26.
//  Copyright © 2016年 ronglian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SynthesizeSingleton.h"
#import "BaseComponent.h"

@interface Common : BaseComponent

//SYNTHESIZE_SINGLETON_FOR_CLASS_HEADER(Common);
+ (Common *)sharedInstance;

//正在请求群组成员信息的群
@property (nonatomic,strong) NSMutableArray *cacheGroupMemberRequestArray;
//临时请求数据缓存 防止无限进入 频繁请求
@property (nonatomic,strong) NSMutableArray *cacheLoadRequestArray;

@property (nonatomic,assign) BOOL isreloadView;
@property (nonatomic,assign) BOOL ishaveSpecialUpdate;//有特别关注更新
@property (nonatomic,assign) BOOL isSendSportMeet;

@property (nonatomic,copy) NSString *historyMessageUrl;//历史消息
@property (nonatomic, copy) NSString * collectSynctime;//上次获取收藏的时间
@property (strong, nonatomic) NSMutableDictionary * FCDynamicDic;//同事圈动态记录 weijy
@property (nonatomic, assign) BOOL isIMMsgMoreSelect;//im消息多选状态
@property (nonatomic, strong) NSMutableArray * moreSelectMsgData;//更多选择的消息
@property (strong, nonatomic) NSMutableDictionary * FCDeleteDic;//通讯录离职删除朋友圈

@property (nonatomic,assign) BOOL isFirstEnterAdd;

@property(nonatomic,strong) NSArray *confRooms;

//检查权限
- (BOOL)checkUserAuth:(NSString *)auth;

/**
 获取网络信号强度，需要开启AFNetworking网络监听
 */
- (NSString *)networkingStatesFromStatebar;

+ (BOOL)isAccordWithSearchConditionName:(NSString *)name withkeyWords:(NSString *)keyWords withFirstLetter:(NSString *)firstLet;


/**
 通过  获取用户昵称 或者群组名称

 @param phone 用户 account或者群组 id
 @return 昵称
 */
- (NSString*)getOtherNameWithPhone:(NSString*)phone;

- (void)deleteOneGroupInfoGroupId:(NSString *)groupId;

//删除会话的数据
- (void)deleteAllMessageOfSession:(NSString*)sessionId;

- (CGSize)widthForContent:(NSString *)text withLableWidth:(CGFloat)width withLableFont:(NSInteger)fontSize;

/**
 音视频呼入铃声播放(铃声播放由应用层处理)
 */
- (void)playAVAudioIncomingCall;

/**
 音视频呼入铃声停止
 */
- (void)stopAVAudio;

/**
 音视频呼入震动(铃声播放由应用层处理)
 */
-(void)startVibrate:(BOOL)isPush;

/**
 停止振动
 */
-(void)stopShakeSoundVibrate;

/**
 检查账号是否被冻结
 */
- (BOOL)checkPointToPiontChatWithAccount:(NSString *)account;

//检查是否是符合权限控制
- (BOOL)checkPointToPiontIsMyFriendWithAccount:(NSString *)account needPrompt:(BOOL)isPromp;



@end
